"""
Primes.py
Finds and prints all prime numbers betwen a given start and end integer

Author: Richard Harrington
Date: 9/20/2013
"""
from math import floor

start=int(input("Input a starting number:\n"))
end=int(input("Input an ending number:\n"))
primes=[]

def is_prime(x):
    if x%2==0:
        return False
    for i in range(3,floor(x/2),2):
        if x%i==0:
            return False
    return True

for i in range(start,end+1):
    if is_prime(i):
        primes.append(i)

print("".join(str(primes)))
    

